import { combineReducers } from 'redux';

import { LISTA_CLIENTES, ADICIONAR_CLIENTE, ATUALIZAR_CLIENTE, DELETAR_CLIENTE } from "../Actions/CRUDCliente" //Import the actions types constant we defined in our actions
import { LISTA_PRODUTOS, ADICIONAR_PRODUTO, ATUALIZAR_PRODUTO, DELETAR_PRODUTO } from "../Actions/CRUDProduto" //Import the actions types constant we defined in our actions

let dataState = { clientes: [], produtos: [], loading: true };

const dataReducer = (state = dataState, action) => {
    switch (action.type) {
        case ADICIONAR_CLIENTE: {
            let clientes = cloneObject(state.clientes) //clone the current state
            clientes.unshift(action.cliente); //add the new quote to the top
            state = Object.assign({}, state, { clientes: clientes });
            return state;
        }

        case LISTA_CLIENTES:
            state = Object.assign({}, state, { clientes: action.clientes, loading: false });
            return state;

        case ATUALIZAR_CLIENTE: {
            let cliente = action.cliente;
            let clientes = cloneObject(state.clientes) //clone the current state
            let index = getIndex(clientes, cliente.id); //find the index of the quote with the quote id passed
            if (index !== -1) {
                clientes[index]['nome'] = cliente.nome;
                clientes[index]['logradouro'] = cliente.logradouro;
                clientes[index]['bairro'] = cliente.bairro;
                clientes[index]['telefone'] = cliente.telefone;


            }
            state = Object.assign({}, state, { clientes: clientes });
            return state;
        }

        case DELETAR_CLIENTE: {
            let clientes = cloneObject(state.clientes) //clone the current state
            let index = getIndex(clientes, action.id); //find the index of the quote with the id passed
            if (index !== -1) clientes.splice(index, 1);//if yes, undo, remove the QUOTE
            state = Object.assign({}, state, { clientes: clientes });
            return state;
        }

        case ADICIONAR_PRODUTO: {
            let produtos = cloneObject(state.produtos) //clone the current state
            produtos.unshift(action.produto); //add the new quote to the top
            state = Object.assign({}, state, { produtos: produtos });
            return state;
        }

        case LISTA_PRODUTOS:
            state = Object.assign({}, state, { produtos: action.produtos, loading: false });
            return state;

        case ATUALIZAR_PRODUTO: {
            let produto = action.produto;
            let produtos = cloneObject(state.produtos) //clone the current state
            let index = getIndex(produtos, produto.id); //find the index of the quote with the quote id passed
            if (index !== -1) {
                produtos[index]['nome'] = produto.nome;
                produtos[index]['categoria'] = produto.categoria;
                
            }
            state = Object.assign({}, state, { produtos: produtos });
            return state;
        }

        case DELETAR_PRODUTO: {
            let produtos = cloneObject(state.produtos) //clone the current state
            let index = getIndex(produtos, action.id); //find the index of the quote with the id passed
            if (index !== -1) produtos.splice(index, 1);//if yes, undo, remove the QUOTE
            state = Object.assign({}, state, { produtos: produtos });
            return state;
        }



        default:
            return state;
    }
};


function cloneObject(object) {
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id) {
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}

// Combine all the reducers
const rootReducer = combineReducers({
    dataReducer
})

export default rootReducer;