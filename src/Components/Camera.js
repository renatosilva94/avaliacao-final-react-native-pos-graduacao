import React from 'react'
import { FileSystem } from 'expo'
import { Text, View, TouchableOpacity, Modal, AsyncStorage  } from 'react-native'
import * as Permissions from 'expo-permissions'
import { Camera } from 'expo-camera'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';

export default class Camera2 extends React.Component {
    
    
    state = {
        photo: null,
        hasCameraPermission: null,
        type: Camera.Constants.Type.back
      }


    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA)
        this.setState({ hasCameraPermission: status === 'granted' })
        

    }

    takePicture = () => {
        if (this.camera) {
            this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved })
        }


    }

    onPictureSaved = async photo => {
       //this.props.callback(photo.uri)
        console.log(photo.uri)
    }

    render() {
        const { hasCameraPermission } = this.state
        if (hasCameraPermission === null) {
            return <View />
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>
        } else {
            return (
                <Modal
                    animationType='slide'
                    transparent={false}
                    visible={this.props.visible}
                    onRequestClose={() => {

                    }}
                >
                    <View style={{ flex: 1, marginTop: 20 }}>



                        <Camera
                            ref={ref => {
                                this.camera = ref
                            }}
                            style={{ flex: 1 }}
                            type={this.state.type}
                        >
                            <View
                                style={{
                                    flex: 1,
                                    backgroundColor: 'transparent',
                                    flexDirection: 'row'
                                }}
                            >
                                <Icon
                                    raised
                                    name='camera'
                                    type='font-awesome'
                                    color='#000'
                                    size={20}
                                    onPress={this.takePicture} />

                                <Icon
                                    raised
                                    name='arrow-left'
                                    type='font-awesome'
                                    color='#000'
                                    size={20}
                                    onPress={this.props.close} />

                            </View>
                        </Camera>
                    </View>
                </Modal>
            )
        }
    }
}