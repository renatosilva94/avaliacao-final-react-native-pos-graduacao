import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert, Button, Vibration
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { adicionarCliente, atualizarCliente } from "../Actions/CRUDCliente";
import styled from 'styled-components'

const DURATION = 1000;

class CadastraClientes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: props.edit ? props.cliente.nome : "",
      cnpj: props.edit ? props.cliente.cnpj : "",
      fantasia: props.edit ? props.cliente.fantasia : "",
      logradouro: props.edit ? props.cliente.logradouro : "",
      bairro: props.edit ? props.cliente.bairro : "",
      telefone: props.edit ? props.cliente.telefone : "",

      documento: '',
      isLoading: false

    };

    this.realizarConsulta = this.realizarConsulta.bind(this);
    this.setDocumento = this.setDocumento.bind(this);

    console.log(this.props);
    this.generateID = this.generateID.bind(this);
    this.adicionarCliente = this.adicionarCliente.bind(this);
  }



  setDocumento(doc) {
    let docFormatado = '';
    let numbers = '0123456789';

    for (let i = 0; i < doc.length; i++) {
      if (numbers.indexOf(doc[i]) > -1) {
        docFormatado = docFormatado + doc[i];
      } else {
        Alert.alert('Apenas números', 'Você só pode digitar números aqui!');
      }
    }
    this.setState({ documento: docFormatado })
  }



  realizarConsulta() {
    const doc = this.state.documento;
    const endpoint = 'https://www.receitaws.com.br/v1/cnpj';

    if (doc === '') {
      Alert.alert('Oops!', 'Nenhum documento informado!');
      return false;
    }

    this.setState({ isLoading: true });

    fetch(`${endpoint}/${doc}`)
      .then(response => {
        return response.json();
      })
      .then(docInfo => {
        this.setState({ isLoading: false, fantasia: docInfo.fantasia, logradouro: docInfo.logradouro, bairro: docInfo.bairro, telefone: docInfo.telefone });

        this.props.navigation.navigate('Resultado', {
          data: docInfo
          
        })
        console.log(docInfo);
      })
      .catch(err => {
        this.setState({ isLoading: false });
        Alert.alert('Oops!', 'Houve um erro inesperado ao realizar a consulta. Pro favor, tente novamente mais tarde!');
        console.log(err);
      });
  }

  generateID() {
    let d = new Date().getTime();
    let id = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (
      c
    ) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == "x" ? r : (r & 0x3) | 0x8).toString(5);
    });

    return id;
  }

  adicionarCliente() {
    if (this.props.navigation.state.params.id) {
      let cliente = { id: this.props.navigation.state.params.id, nome: this.state.nome, logradouro: this.state.logradouro, bairro: this.state.bairro, telefone: this.state.telefone };

      this.props.atualizarCliente(cliente);
      console.log(cliente);
      Vibration.vibrate(DURATION);

      Alert.alert("Atualizado Com Sucesso");
    } else {
      let id = this.generateID();
      let cliente = { id: id, nome: this.state.nome, logradouro: this.state.logradouro, bairro: this.state.bairro, telefone: this.state.telefone };
      this.props.adicionarCliente(cliente);
      Vibration.vibrate(DURATION);

      Alert.alert("Adicionado Com Sucesso");
    }

    Actions.pop();
  }

  render() {

    const data = this.props.navigation.getParam('data', null);

    

    return (
      <StyledView style={{ flex: 1 }}>
        <StyledTextTitle>Cadastro de Clientes</StyledTextTitle>

        <StyledTextInput placeholder="00.000.000/0000.00" onChangeText={(doc) => this.setDocumento(doc)} maxLength={14} />


        

<StyledButton
          
          onPress={this.realizarConsulta}
        >
          <StyledTextButton>
            Consultar
          </StyledTextButton>
        </StyledButton>

        <StyledTextInput
          onChangeText={text => this.setState({ nome: text })}
          placeholder={"Nome"}
          autoFocus={true}
          value={this.state.nome}
        />



        <StyledTextInput
          onChangeText={text => this.setState({ fantasia: text })}
          placeholder={"Nome Fantasia"}
          value={this.state.fantasia}
          
        />

<StyledTextInput
          onChangeText={text => this.setState({ logradouro: text })}
          placeholder={"Logradouro"}
          value={this.state.logradouro}
          
        />

<StyledTextInput
          onChangeText={text => this.setState({ bairro: text })}
          placeholder={"Bairro"}
          value={this.state.bairro}
          
        />


<StyledTextInput
          onChangeText={text => this.setState({ telefone: text })}
          placeholder={"Telefone"}
          value={this.state.telefone}
          
        />


        <StyledButton
          disabled={this.state.nome.length > 0 ? false : true}
          onPress={this.adicionarCliente}
        >
          <StyledTextButton>
            Salvar
          </StyledTextButton>
        </StyledButton>
      </StyledView>
    );
  }
}

export default connect(
  null,
  { adicionarCliente, atualizarCliente }
)(CadastraClientes);

const StyledButton = styled.TouchableOpacity`
    background-color: #fff;
    font-size: 60;
    border-radius: 5px;   
`


const StyledTextButton = styled(Text)`
    text-align: center;
    color: #705714;
    font-size: 20;
    margin: 10px;
`
const StyledView = styled.View`
background-color: #705714;
align-items: center;
`

const StyledTextTitle = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 30;
    margin: 10px;
    font-weight: bold;
`

const StyledTextInput = styled(TextInput)`
text-align: center;
font-size: 15;
color: #fff;
`



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#705714"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "#000"
  },
  saveBtn: {
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#705714"
  },

  buttonText: {
    fontWeight: "500"
  }
});
