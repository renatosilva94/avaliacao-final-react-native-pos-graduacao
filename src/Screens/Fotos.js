import React, { Component } from "react";
import styled from "styled-components";
import { AsyncStorage, Image } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'

export default class Fotos extends Component {

  constructor(props) {
    super(props);
    this.getStorageImg();
  }

  state = {
    base64: ''
  }

  getStorageImg = async () => {
    const img = await AsyncStorage.getItem('img')
    this.setState({ base64: img })
    this.render();
  }

  render() {
    const img64 = `data:image/jpg;base64,${this.state.base64}`;
    console.log(img64)
    return (
      <Container>
        <Image style={{
          width: 200,
          height: 300,
          resizeMode: 'contain',
        }} source={{ uri: img64 }} />
        <BtnVoltar onPress={() => this.getStorageImg}>
          <WellCome>Salvar</WellCome>
        </BtnVoltar>


        <Icon
          raised
          name='image'
          type='font-awesome'
          color='#000'
          size={20}
          onPress={() => Actions.galeria()} />

      </Container>
    );
  }

}

const Container = styled.View`
  flex: 1;
  background-color: #000;
  align-items: center;
`;


const WellCome = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #fff;
`;


const BtnVoltar = styled.TouchableOpacity`
  margin-top: 15px;
  border: 2px solid #fff;
  border-radius: 5px;
  padding: 5px;
`;