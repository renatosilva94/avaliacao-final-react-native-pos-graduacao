import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  FlatList,
  ActivityIndicator, TouchableHighlight, ActionSheetIOS
} from 'react-native';

import { Icon } from 'react-native-elements'

import { atualizarCliente } from '../Actions/CRUDCliente'


import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';

import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux'

import Data from '../JSON/listaCliente.json'


import { listarCliente } from '../Actions/CRUDCliente'
import { connect } from 'react-redux';

import { Root } from 'native-base'

import * as ReduxActions from '../Actions/CRUDCliente'; //Import your actions
import CadastraClientes from './CadastraClientes';
import styled from 'styled-components'


const BUTTONS = [
  "Editar",
  "Deletar",
  'Cancelar',
];

// import { Actions } from 'react-native-router-flux';
const CANCEL_INDEX = 2;


class ListagemClientes extends Component {




  constructor(props) {
    super(props);

    this.state = {
      nome: (props.edit) ? props.cliente.nome : "",
    };

    this.atualizarCliente = this.atualizarCliente.bind(this);

    this.mostrarClientes = this.mostrarClientes.bind(this);
    //this.mostrarOpcoes = this.mostrarOpcoes.bind(this);
  }

  componentDidMount() {

    var _this = this;
    //Check if any data exist
    AsyncStorage.getItem('data', (err, data) => {
      //if it doesn't exist, extract from json file
      //save the initial data in Async
      if (data === null) {
        AsyncStorage.setItem('data', JSON.stringify(Data.clientes));
        _this.props.listarCliente();
      } else {
        this.props.listarCliente(); //call our action
      }
    });

  }



  // mostrarOpcoes = () => {

  //   const options = ['Deletar', 'Salvar', 'Cancelar'];
  //   const destructiveButtonIndex = 0;
  //   const cancelButtonIndex = 2;

  //   this.props.showActionSheetWithOptions(
  //     {
  //       options,
  //       cancelButtonIndex,
  //       destructiveButtonIndex,
  //     },
  //     buttonIndex => {
  //       if (buttonIndex === 0) Actions.atualizarCliente({ cliente: cliente, edit: true, title: "Editar Cliente" })
  //       else if (buttonIndex === 1) this.props.deletarCliente(cliente.id)  
  //     },
  //   );
  // };

  render() {

    if (this.props.loading) {
      return (
        <StyledViewActivityIndicator style={{flex:1}}>
          <ActivityIndicator animating={true} />
        </StyledViewActivityIndicator>
      );
    } else {
      return (

        <StyledView style={{ flex: 1 }}>
          <StyledTextTitle>
            Listagem de Clientes
      </StyledTextTitle>

          <FlatList
            ref='listRef'
            data={this.props.clientes}
            renderItem={this.mostrarClientes}
            keyExtractor={(item, index) => index}

          />





        </StyledView>

      );
    }
  }

  openModal = () => {
    this.setState({
      isVisible: !this.state.isVisible
    })
  }

  atualizarCliente() {
    // let cliente = this.props.cliente;
    // cliente['nome'] = this.state.nome;
    // this.props.atualizarCliente(cliente);
    // Alert.alert('Atualizado Com Sucesso')
    // console.log('1');
  }

  mostrarClientes({ item, index }) {
    return (
      <StyledViewRow>

        <StyledTextContent>
         Nome: {item.nome} 
        </StyledTextContent>

        <StyledTextContent>
          Endereço: {item.logradouro},{item.bairro}
        </StyledTextContent>

        <StyledTextContent>
          Telefone: {item.telefone}
        </StyledTextContent>

        <Icon
          raised
          name='pencil'
          type='font-awesome'
          color='#f50'
          size={20}
          onPress={() => Actions.cadastracliente({ id: item.id, nome: item.nome, title: "Editar Cliente" })}
        />



        <Icon
          raised
          name='trash'
          type='font-awesome'
          color='#0000ff'
          size={20}
          onPress={() => this.props.deletarCliente(item.id)} />











      </StyledViewRow>
    )
  }


  //onPress={() => this.mostrarOpcoes(item)} underlayColor='rgba(0,0,0,.2)'>

};

function mapStateToProps(state, props) {
  return {
    loading: state.dataReducer.loading,
    clientes: state.dataReducer.clientes
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ReduxActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListagemClientes);

const StyledView = styled.View`
background-color: #f58300;
align-items: center;
`

const StyledTextTitle = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 30;
    margin: 10px;
    font-weight: bold;
`
const StyledTextContent = styled(Text)`
    font-size: 14;
    margin-top: 5px;
    font-weight: bold;
`

const StyledViewRow = styled.View`
background-color: #fff;
align-items: center;
padding: 10px;
margin: 3px
`

const StyledViewActivityIndicator = styled.View`
background-color: #f58300;
align-items: center;
`
