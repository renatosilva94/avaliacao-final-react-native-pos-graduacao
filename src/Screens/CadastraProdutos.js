import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity, Alert, Picker, Clipboard, Vibration
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { adicionarProduto, atualizarProduto } from '../Actions/CRUDProduto'
import { Icon } from 'react-native-elements'
import BarCode from '../Components/BarCode'
import styled from 'styled-components'

const DURATION = 1000;

class CadastraProdutos extends Component {


  constructor(props) {
    super(props);

    this.state = {
      nome: (props.edit) ? props.produto.nome : "",
      categoria: (props.edit) ? props.produto.categoria : "",
      codBarras: (props.edit) ? props.produto.codBarras : "",
      clipboardContent: null,

    };
    this.generateID = this.generateID.bind(this);
    this.adicionarProduto = this.adicionarProduto.bind(this);
  }

  readFromClipboard = async () => {
    const clipboardContent = await Clipboard.getString();
    this.setState({ codBarras: clipboardContent });
  };



  generateID() {
    let d = new Date().getTime();
    let id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(5);
    });

    return id;
  }

  adicionarProduto() {

    if (this.props.navigation.state.params.id) {
      let produto = { id: this.props.navigation.state.params.id, nome: this.state.nome, categoria: this.state.categoria, codBarras: this.state.codBarras };

      this.props.atualizarProduto(produto);
      Vibration.vibrate(DURATION);

      Alert.alert('Atualizado Com Sucesso')
    } else {
      let id = this.generateID();
      let produto = { "id": id, "nome": this.state.nome, "categoria": this.state.categoria, "codBarras": this.state.codBarras };
      this.props.adicionarProduto(produto);
      console.log(produto);
      Vibration.vibrate(DURATION);

      Alert.alert('Adicionado Com Sucesso')
    }

    Actions.pop();
  }



  render() {


    return (



      <StyledView style={{ flex: 1 }}>


        <StyledTextTitle>
          Cadastro de Produtos
      </StyledTextTitle>

        <StyledTextInput
          onChangeText={(text) => this.setState({ nome: text })}
          placeholder={"Nome"}
          value={this.state.nome}
        />

        <StyledPicker
          selectedValue={this.state.categoria}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ categoria: itemValue })
          }>
          <Picker.Item label="Pizza" value="pizza" />
          <Picker.Item label="Bauru" value="bauru" />
          <Picker.Item label="Fruta" value="fruta" />

        </StyledPicker>


        <StyledTextInput
          onChangeText={(text) => this.setState({ codBarras: text })}
          placeholder={"Código de Barras"}
          value={this.state.codBarras}

        />

        <Icon
          raised
          name='camera'
          type='font-awesome'
          color='#000'
          size={20}
          onPress={() => Actions.leitorcodbarras()}
        />

        <Icon
          raised
          name='clone'
          type='font-awesome'
          color='#000'
          size={20}
          onPress={() => this.readFromClipboard()}
        />



        <StyledButton
          disabled={(this.state.nome.length > 0) ? false : true}
          onPress={this.adicionarProduto}>
          <StyledTextButton>
            Salvar
                    </StyledTextButton>
        </StyledButton>

      </StyledView>
    )
  }

}

export default connect(null, { adicionarProduto, atualizarProduto })(CadastraProdutos);

const StyledButton = styled.TouchableOpacity`
    background-color: #fff;
    font-size: 60;
    border-radius: 5px;   
`

const StyledTextButton = styled(Text)`
    text-align: center;
    color: #a626a6;
    font-size: 20;
    margin: 10px;
`
const StyledView = styled.View`
background-color: #a626a6;
align-items: center;
`

const StyledTextTitle = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 30;
    margin: 10px;
    font-weight: bold;
`

const StyledTextInput = styled(TextInput)`
text-align: center;
font-size: 15;
color: #fff;
`

const StyledPicker = styled(Picker)`
font-size: 60;
color: #fff;
width: 150
`

