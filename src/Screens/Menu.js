import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styled from 'styled-components'

const Menu = () => {
  return (
    <StyledView style={{flex:1}}>
      <StyledTextTitle>
        Supermercado
      </StyledTextTitle>
      <StyledButton  onPress={() => Actions.cadastraproduto()}><StyledText>Cadastro de Produtos</StyledText></StyledButton>
      <StyledButton  onPress={() => Actions.cadastracliente()}><StyledText>Cadastro de Clientes</StyledText></StyledButton>
      <StyledButton  onPress={() => Actions.listagemproduto()}><StyledText>Listagem de Produtos</StyledText></StyledButton>
      <StyledButton  onPress={() => Actions.listagemcliente()}><StyledText>Listagem de Clientes</StyledText></StyledButton>
      <StyledButton  onPress={() => Actions.foto()}><StyledText>Foto do Perfil</StyledText></StyledButton>
      <StyledButton  onPress={() => Actions.album()}><StyledText>Album</StyledText></StyledButton>

      


    </StyledView>
  );
}

const StyledButton = styled.TouchableOpacity`
    background-color: #000;
    font-size: 20;
    
`

const StyledText = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 20;
    margin: 10px;
`

const StyledView = styled.View`
background-color: #000;
`

const StyledTextTitle = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 30;
    margin: 10px;
    font-weight: bold;
`



export default Menu;