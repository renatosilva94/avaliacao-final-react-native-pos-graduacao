import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  FlatList,
  ActivityIndicator, TouchableHighlight, ActionSheetIOS
} from 'react-native';

import { Icon } from 'react-native-elements'


import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';

import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux'

import Data from '../JSON/listaProduto.json'


import { listarProduto } from '../Actions/CRUDProduto'
import { connect } from 'react-redux';

import { Root } from 'native-base'

import * as ReduxActions from '../Actions/CRUDProduto'; //Import your actions
import styled from 'styled-components'


const BUTTONS = [
  "Editar",
  "Deletar",
  'Cancelar',
];

// import { Actions } from 'react-native-router-flux';
const CANCEL_INDEX = 2;

class ListagemProdutos extends Component {


  constructor(props) {
    super(props);

    this.state = {};

    this.mostrarProdutos = this.mostrarProdutos.bind(this);
    //this.mostrarOpcoes = this.mostrarOpcoes.bind(this);
  }

  componentDidMount() {

    var _this = this;
    //Check if any data exist
    AsyncStorage.getItem('dataProd', (err, data) => {
      //if it doesn't exist, extract from json file
      //save the initial data in Async
      if (data === null) {
        AsyncStorage.setItem('dataProd', JSON.stringify(Data.produtos));
        _this.props.listarProduto();
      } else {
        this.props.listarProduto(); //call our action
      }
    });

  }



  // mostrarOpcoes = () => {

  //   const options = ['Deletar', 'Salvar', 'Cancelar'];
  //   const destructiveButtonIndex = 0;
  //   const cancelButtonIndex = 2;

  //   this.props.showActionSheetWithOptions(
  //     {
  //       options,
  //       cancelButtonIndex,
  //       destructiveButtonIndex,
  //     },
  //     buttonIndex => {
  //       if (buttonIndex === 0) Actions.atualizarCliente({ cliente: cliente, edit: true, title: "Editar Cliente" })
  //       else if (buttonIndex === 1) this.props.deletarCliente(cliente.id)  
  //     },
  //   );
  // };

  render() {

    if (this.props.loading) {
      return (
        <StyledViewActivityIndicator>
          <ActivityIndicator animating={true} />
        </StyledViewActivityIndicator>
      );
    } else {
      return (

        <StyledView style={{flex: 1}}>
          <StyledTextTitle>
            Listagem de Produtos
      </StyledTextTitle>

          <FlatList
            ref='listRef2'
            data={this.props.produtos}
            renderItem={this.mostrarProdutos}
            keyExtractor={(item, index) => index}

          />





        </StyledView>

      );
    }
  }

  openModal = () => {
    this.setState({
        isVisible: !this.state.isVisible
    })
}

  mostrarProdutos({ item, index }) {
    return (
      <StyledViewRow>

        <StyledTextContent>
         Nome: {item.nome} 

        </StyledTextContent>

        <StyledTextContent>
         Categoria: {item.categoria}
        </StyledTextContent>
        

        <Icon
          raised
          name='pencil'
          type='font-awesome'
          color='#f50'
          size={20}
          onPress={() => Actions.cadastraproduto({id: item.id, nome: item.nome, categoria: item.categoria, title: "Editar Produto"})}
          />

        <Icon
          raised
          name='trash'
          type='font-awesome'
          color='#0000ff'
          size={20}
          onPress={() => this.props.deletarProduto(item.id)} />











      </StyledViewRow>
    )
  }


  //onPress={() => this.mostrarOpcoes(item)} underlayColor='rgba(0,0,0,.2)'>

};

function mapStateToProps(state, props) {
  return {
    loading: state.dataReducer.loading,
    produtos: state.dataReducer.produtos
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ReduxActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListagemProdutos);


const StyledView = styled.View`
background-color: #511600;
align-items: center;
`

const StyledTextTitle = styled(Text)`
    text-align: center;
    color: #fff;
    font-size: 30;
    margin: 10px;
    font-weight: bold;
`
const StyledTextContent = styled(Text)`
    font-size: 14;
    margin-top: 5px;
    font-weight: bold;
`

const StyledViewRow = styled.View`
background-color: #fff;
align-items: center;
padding: 10px;
margin: 3px
`

const StyledViewActivityIndicator = styled.View`
background-color: #511600;
align-items: center;
`


