export const LISTA_PRODUTOS = 'LISTA_PRODUTOS';
export const ADICIONAR_PRODUTO = 'ADICIONAR_PRODUTO';
export const ATUALIZAR_PRODUTO = 'ATUALIZAR_PRODUTO';
export const DELETAR_PRODUTO = 'DELETAR_PRODUTO';

import {AsyncStorage} from "react-native";


// Adicionar Produto
export function adicionarProduto(produto){
    return (dispatch) => {
        AsyncStorage.getItem('dataProd', (err, produtos) => {
            if (produtos !== null){
                produtos = JSON.parse(produtos);
                produtos.unshift(produto); 
                AsyncStorage.setItem('dataProd', JSON.stringify(produtos), () => {
                    dispatch({type: ADICIONAR_PRODUTO, produto:produto});
                });
            }
        });
    };
}

//Listar Produto
export function listarProduto(){
    return (dispatch) => {
        AsyncStorage.getItem('dataProd', (err, produtos) => {
            if (produtos !== null){
                dispatch({type: LISTA_PRODUTOS, produtos:JSON.parse(produtos)});
            }
        });
    };
}

// Atualizar Produto
export function atualizarProduto(produto){
    return (dispatch) => {
        AsyncStorage.getItem('dataProd', (err, produtos) => {
            if (produtos !== null){
                produtos = JSON.parse(produtos);
                var index = getIndex(produtos, produto.id); 
                if (index !== -1) {
                    produtos[index]['produto'] = produto.nome;
                    produtos[index]['categoria'] = produto.categoria;

                }
                AsyncStorage.setItem('dataProd', JSON.stringify(produtos), () => {
                    dispatch({type: ATUALIZAR_PRODUTO, produto:produto});
                });
            }
        });
    };
}

//Deletar Produto
export function deletarProduto(id){
    return (dispatch) => {
        AsyncStorage.getItem('dataProd', (err, produtos) => {
            if (produtos !== null){
                produtos = JSON.parse(produtos);

                var index = getIndex(produtos, id); //find the index of the quote with the id passed
                if(index !== -1) produtos.splice(index, 1);//if yes, undo, remove the QUOTE
                AsyncStorage.setItem('dataProd', JSON.stringify(produtos), () => {
                    dispatch({type: DELETAR_PRODUTO, id:id});
                });
            }
        });
    };
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}