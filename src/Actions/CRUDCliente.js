export const LISTA_CLIENTES = 'LISTA_CLIENTES';
export const ADICIONAR_CLIENTE = 'ADICIONAR_CLIENTE';
export const ATUALIZAR_CLIENTE = 'ATUALIZAR_CLIENTE';
export const DELETAR_CLIENTE = 'DELETAR_CLIENTE';

import {AsyncStorage} from "react-native";


// Adicionar Cliente
export function adicionarCliente(cliente){
    return (dispatch) => {
        AsyncStorage.getItem('data', (err, clientes) => {
            if (clientes !== null){
                clientes = JSON.parse(clientes);
                clientes.unshift(cliente); 
                AsyncStorage.setItem('data', JSON.stringify(clientes), () => {
                    dispatch({type: ADICIONAR_CLIENTE, cliente:cliente});
                });
            }
        });
    };
}

//Listar Data
export function listarCliente(){
    return (dispatch) => {
        AsyncStorage.getItem('data', (err, clientes) => {
            if (clientes !== null){
                dispatch({type: LISTA_CLIENTES, clientes:JSON.parse(clientes)});
            }
        });
    };
}

// Atualizar Cliente
export function atualizarCliente(cliente){
    return (dispatch) => {

        AsyncStorage.getItem('data', (err, clientes) => {
            if (clientes !== null){

                clientes = JSON.parse(clientes);
                var index = getIndex(clientes, cliente.id); 
                if (index !== -1) {
                    clientes[index]['nome'] = cliente.nome;
                    clientes[index]['logradouro'] = cliente.logradouro;
                    clientes[index]['bairro'] = cliente.bairro;
                    clientes[index]['telefone'] = cliente.telefone;

                }
                AsyncStorage.setItem('data', JSON.stringify(clientes), () => {
                    dispatch({type: ATUALIZAR_CLIENTE, cliente:cliente});
                });
            }
        });
    };
}

//Deletar Cliente
export function deletarCliente(id){
    return (dispatch) => {
        AsyncStorage.getItem('data', (err, clientes) => {
            if (clientes !== null){
                clientes = JSON.parse(clientes);

                var index = getIndex(clientes, id); //find the index of the quote with the id passed
                if(index !== -1) clientes.splice(index, 1);//if yes, undo, remove the QUOTE
                AsyncStorage.setItem('data', JSON.stringify(clientes), () => {
                    dispatch({type: DELETAR_CLIENTE, id:id});
                });
            }
        });
    };
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}