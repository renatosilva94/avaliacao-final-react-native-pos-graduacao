import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Menu from './src/Screens/Menu'
import CadastraProdutos from './src/Screens/CadastraProdutos'
import CadastraClientes from './src/Screens/CadastraClientes'
import ListagemProdutos from './src/Screens/ListagemProdutos'
import ListagemClientes from './src/Screens/ListagemClientes'
import Fotos from './src/Screens/Fotos'
import Camera from './src/Components/Camera'
import ImagePicker from './src/Components/ImagePicker'
import BarCode from './src/Components/BarCode'
import Album from './src/Screens/Album'

import { Provider } from 'react-redux';
import store from './src/store'



const App = () => {
  return (


    <Provider store={store}>
      <Router>
        <Scene key="root">
          <Scene key="menu"
            component={Menu}
            title="Menu"
            initial
          />

<Scene
            key="leitorcodbarras"
            component={BarCode}
            title="Leitor de Cod. de Barras"
          />



          <Scene
            key="cadastraproduto"
            component={CadastraProdutos}
            title="Cadastra Produto"
          />

          <Scene
            key="cadastracliente"
            component={CadastraClientes}
            title="Cadastra Cliente"
          />

<Scene
            key="cadastracliente"
            component={CadastraClientes}
            title="Cadastra Cliente"
          />

         

          <Scene
            key="listagemproduto"
            component={ListagemProdutos}
            title="Listagem Produtos"
          />

          <Scene
            key="album"
            component={Album}
            title="Album de Fotos "
          />


          <Scene
            key="listagemcliente"
            component={ListagemClientes}
            title="Listagem Clientes"
          />

          <Scene
            key="foto"
            component={Fotos}
            title="Fotos"
          />

<Scene
            key="camera"
            component={Camera}
            title="Camera"
          />

<Scene
            key="galeria"
            component={ImagePicker}
            title="Galeria"
          />




        </Scene>
      </Router>
    </Provider>

  );
}

export default App;